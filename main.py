#!/usr/bin/env python3

import sys

import gi

gi.require_version("Gtk", '4.0')
gi.require_version("Adw", "1")
from gi.repository import Adw, Gtk  # pyright: ignore [reportGeneralTypeIssues]


class MainWindow(Gtk.ApplicationWindow):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_title("Simple Calculator")

        self.box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.box.set_margin_start(10)
        self.box.set_margin_end(10)
        self.box.set_margin_top(10)
        self.box.set_margin_bottom(10)
        self.set_child(self.box)

        self.display = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.box.append(self.display)

        self.output = Gtk.Frame()
        self.output.set_hexpand(True)
        self.display.append(self.output)

        self.output_label = Gtk.Label(label="0")
        self.output_label.set_halign(Gtk.Align.END)
        self.output.set_child(self.output_label)

        self.backspace = Gtk.Button(icon_name="edit-clear-symbolic")
        self.backspace.connect("clicked", self.clicked_backspace)
        self.display.append(self.backspace)

        self.numpad = Gtk.Grid()
        self.numpad_spacing = 4
        self.numpad.set_vexpand(True)
        self.numpad.set_row_spacing(self.numpad_spacing)
        self.numpad.set_column_spacing(self.numpad_spacing)
        self.numpad.set_row_homogeneous(True)
        self.numpad.set_column_homogeneous(True)
        self.box.append(self.numpad)

        buttons = [
            [ "7", "8", "9" ],
            [ "4", "5", "6" ],
            [ "1", "2", "3" ],
            [ ".", "00", "0" ],
            [ "+", "-", "//" ],
            [ "*", "/", "^" ],
            [ "mod", "AC", "=" ]
        ]

        for i, row in enumerate(buttons):
            wid = 1 if i not in [4, 5] else 1.5
            for j, label in enumerate(row):
                button = Gtk.Button(label=label)
                button.connect("clicked", self.numpad_clicked)
                self.numpad.attach(button, j, i, wid, 1)

    def clicked_backspace(self, _):
        edit = self.output_label.get_label()[:-1]
        if edit == "" or edit.startswith("ERROR: "):
            self.output_label.set_label("0")
        else:
            self.output_label.set_label(edit)

    def numpad_clicked(self, button):
        expr = button.get_label()
        label = self.output_label.get_label()
        label = "" if label == "0" or label.startswith("ERROR: ") else label

        match expr:
            case "=":
                if label == "":
                    label = "0"
                item = label.replace("^", "**")
                result = 0
                try:
                    result = eval(item)
                except SyntaxError:
                    self.output_label.set_label("ERROR: Invalid input")
                except ZeroDivisionError:
                    self.output_label.set_label("ERROR: Division by zero")
                else:
                    self.output_label.set_label(str(result))
            case "00":
                if label != "":
                    self.output_label.set_label(label + expr)
            case "AC":
                self.output_label.set_label("0")
            case "mod":
                self.output_label.set_label(label + "%")
            case _:
                self.output_label.set_label(label + expr)


class CalculatorApp(Adw.Application):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.connect("activate", self.on_activate)

    def on_activate(self, app):
        self.win = MainWindow(application=app)
        self.win.present()


def main():
    app = CalculatorApp(application_id="org.codeberg.Grafcube.CalculatorApp")
    app.run(sys.argv)


if __name__ == "__main__":
    main()
